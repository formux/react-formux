const fs = require("fs");
const rimraf = require("rimraf");

const { resolve, join } = require("path");
const { render } = require("sass");


const CSS_SASS_REGEX = /\.s?css$/;

function searchFiles(path = "./src", query = CSS_SASS_REGEX) {
    const entries = fs.readdirSync(path, { withFileTypes: true });

    const files = entries
        .filter(file => !file.isDirectory())
        .map(file => ({ ...file, path: join(path, file.name) }))
        .filter(({ path }) => query.test(path));

    const folders = entries.filter(folder => folder.isDirectory());

    for (const folder of folders)
        files.push(...getFiles(`${path}${folder.name}/`));

    return files;
}

render({
    file: resolve("src/index.scss"),
    outFile: "styles/formux.css"
}, (err, { css }) => {
    if(err) throw Error(err);

    rimraf.sync("styles")
    if(!fs.existsSync("styles")) {
        fs.mkdirSync("styles")
    }

    fs.writeFileSync("styles/formux.css", css, { encoding: "utf-8" });
    
    for (const file of searchFiles()) {
        if(file.path.endsWith("src/index.scss")) file.name = "formux.scss";
        fs.copyFileSync(file.path, join("styles", file.name));
    };
});
